#####
rslib
#####

This is a minimally invasive userspace port of Linux's Reed Solomon
library.


Usage
#####

The API is comparable to other RS libraries out there, and one may
refer to uses in the Linux kernel.

The symbol size is limited to 16 bits.

It should be noted that prior to calling the `encode_rs{8,16}()`
functions, the "parity" array should be zero-initialized.

License
#######

See the COPYING file along with this one.

